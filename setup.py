from setuptools import setup

setup(
    name='Hash Crawler',
    version='1.0',
    description='A Python Hash Crawler',
    author='Jeff Ussing',
    author_email='Jeff.Ussing@RedDeltaGroup.com',
    url='http://RedDeltaGroup.com',
)
